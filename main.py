from typing import NamedTuple, Dict, List, Optional


class SpendResult(NamedTuple):
    is_Successful: bool
    account_balance: int


class Account(object):
    def __init__(self, name: str, init_balance: int) -> None:
        self.name = name
        self.init_balance = init_balance

    def credit(self, value: int) -> bool:
        if value <= 0:
            return False
        self.init_balance += value
        return True

    def spend(self, value: int) -> SpendResult:
        if value <= 0:
            return SpendResult(False, self.init_balance)
        if self.init_balance < value:
            return SpendResult(False, self.init_balance)
        self.init_balance -= value
        return SpendResult(True, self.init_balance)


class Bank(object):
    def __init__(self) -> None:
        self.account_name_to_customer: Dict[str, Customer] = {}

    def add_customers(self, customers: List[Customer]) -> List[bool]:
        result: List[bool] = []
        for customer in customers:
            if customer.name in self.account_name_to_customer:
                result.append(False)
            else:
                self.account_name_to_customer[customer.name] = customer
                result.append(True)
        return result

    def transfer_funds(
        self, from_customer_name: str, to_customer_name: str, amount: int
    ) -> bool:
        if (
            from_customer_name not in self.account_name_to_customer
            or to_customer_name not in self.account_name_to_customer
        ):
            return False

        from_account = self.account_name_to_customer[from_customer_name].primary_account
        to_account = self.account_name_to_customer[to_customer_name].primary_account

        if from_account is None or to_account is None:
            return False

        if from_account.spend(amount).is_Successful:
            to_account.credit(amount)
            return True
        return False


class Customer(object):
    def __init__(self, name: str) -> None:
        self.name = name
        self.accounts: List[Account] = []
        self.primary_account_name: str = Optional[None]

    def get_account_by_name(self, name: str) -> Optional[Account]:
        for account in self.accounts:
            if account.name == name:
                return account
        return None

    def add_account(self, account: Account) -> None:
        self.accounts.append(account)

    @property
    def primary_account(self) -> Optional[Account]:
        if self.primary_account_name == None:
            return None
        return self.get_account_by_name(self.primary_account_name)


def sum_of_two_numbers(a: int, b: int) -> int:
    return a + b


def main() -> None:
    print(sum_of_two_numbers(2, 3))


if __name__ == "__main__":
    main()
